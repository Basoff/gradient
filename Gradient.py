import matplotlib.pyplot as plt
import numpy as np

def col (c1, c2, t):
    return (1-t)*c1 + t*c2

def norm (color):
    if int(color) >= 0 and int(color) <= 255:
        return int(color)
    elif color < 0:
        return 0
    elif color > 255:
        return 255

size = 100
image = np.zeros((size, size, 3), dtype="uint8")
assert image.shape[0] == image.shape[1] #проверка на квадратность изображения, если каким-то образом это будет не так, выдаст ошибку.

r,g,b = input("Введите RGB-код первого цвета:").split()

color1 = [norm(r), norm(g), norm(b)]
r,g,b = input("Введите RGB-код второго цвета:").split()
color2 = [norm(r), norm(g), norm(b)]

for i, v in enumerate(np.linspace(0, 1, image.shape[0]+image.shape[1]-1)):
    r = col(color1[0], color2[0], v)
    g = col(color1[1], color2[1], v)
    b = col(color1[2], color2[2], v)
    image[:, i:, :] = [r,g,b]
    image[i:, :, :] = [r,g,b]
    for j in range(i):
        image[j:, i-j:, :] = [r,g,b]
        

plt.figure(1)
plt.imshow(image)
plt.show()

input("Нажмите Enter чтобы выйти.")
